import React from 'react';
import Header from './components/header';
import HomePage from './pages/HomePage';
import Footer from './components/footer';
import ErrorBoundary from './components/ErrorBoundry';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import SearchResult from './pages/searchResult';


import {
  createInstance,
  OptimizelyFeature,
  OptimizelyProvider,
  withOptimizely
} from '@optimizely/react-sdk';


const optimizely = createInstance({
  sdkKey: 'WtzosdS7WAieFsSSGJqXQh'
});


function App() {
  return (
    <div className="App" data-testid="app-test">
       <OptimizelyProvider
        optimizely={optimizely}
        user={{
          id: 'riya',
        }}
      >
      <Header />
      <ErrorBoundary>
      <BrowserRouter>
        <Switch>
          <Route  path="/" component={HomePage} exact />
          <Route exact path="/search" component={SearchResult} />
        </Switch>
      </BrowserRouter>
      </ErrorBoundary>
      <Footer />
      </OptimizelyProvider>
    </div>
  );
}

export default App;
