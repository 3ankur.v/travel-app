import React from "react"
import CarouselHome from "../components/Carousel";
import QuickSearch from "../components/home/quickSearch";
import HomeView from "../components/home";

function HomePage(props) {

 return (
  <div className="homepage-container">
   <CarouselHome />
   <QuickSearch  {...props}/>
   <HomeView />
  </div>
 )
}
export default HomePage;