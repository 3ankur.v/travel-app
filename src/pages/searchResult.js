import React from "react";
import MarkSearchResult from "../components/markSearchResult";
import CarouselHome from "../components/Carousel";
import CarouselHotel from "../components/common/CarouselHotel";
import Rater from "react-rater";
import {
  OptimizelyFeature,
} from '@optimizely/react-sdk';

const searchResult = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

function SearchResult() {

  return (
    <div>
      {/* <header class="fixed flex w-full bg-white border-b items-center justify-between flex-wrap p-5 m-auto top-0 animated"></header> */}

      <div class="flex flex-row bg-gray-200 px-5">
        <div class="text-gray-700 text-center bg-gray-400 px-4 py-2 m-2 rounded-full hover:bg-white hover:border-gray-100 hover:shadow-outline hover:border-gray-100">
          <label>Dates</label>
        </div>
        <div class="text-gray-700 text-center bg-gray-400 px-4 py-2 m-2 rounded-full hover:bg-white hover:border-gray-100 ">
          <label>1 Guest</label>
        </div>
        <div class="text-gray-700 text-center bg-gray-400 px-4 py-2 m-2 rounded-full hover:bg-white hover:border-gray-100 ">
          <label>Home Type</label>
        </div>
        <div class="text-gray-700 text-center bg-gray-400 px-4 py-2 m-2 rounded-full hover:bg-white hover:border-gray-100 ">
          <label>Price</label>
        </div>

        <div class="text-gray-700 text-center bg-gray-400 px-4 py-2 m-2 rounded-full hover:bg-white hover:border-gray-100 ">
          <label>Instant book</label>
        </div>

        <div class="text-gray-700 text-center bg-gray-400 px-4 py-2 m-2 rounded-full hover:bg-white hover:border-gray-100 ">
          <label>More Filter</label>
        </div>

      </div>


      <div className="mx-auto flex">

        <div class="left-container w-2/3 p-1 m-1 px-5">
          <div class="flex flex-wrap overflow-hidden sm:-mx-1">

            {
              searchResult.map((itm) => {
                return (
                  <div key={itm} class="w-full overflow-hidden sm:my-1 sm:px-1 md:w-1/3 lg:w-1/3 ">
                    <div className="hotel-img-container" >
                      <CarouselHotel imagesList={[
                        {
                          id: 1,
                          url: 'http://makent.trioangle.com/images/rooms/10043/01600780409_450x250.jpg',
                          name: 'test-img'
                        },
                        {
                          id: 2,
                          url: 'http://makent.trioangle.com/images/rooms/10043/01600780434_450x250.jpg',
                          name: 'test-img'
                        },
                        {
                          id: 3,
                          url: 'http://makent.trioangle.com/images/rooms/10043/01600780499_450x250.jpg',
                          name: 'test-img'
                        },
                      ]} />
                    </div>
                    <div className="basic_details">
                    <div class="p-3">
    <h3 class="mr-10 text-sm truncate-2nd">
      <a class="hover:text-blue-500" href="/huawwei-p20-pro-complete-set-with-box-a.7186128376">Some Title Here</a>
    </h3>
    <div class="f justify-between">
      <p class="text-xs text-gray-500">Quezon City, Metro Manila</p>
      <div><Rater total={5} rating={2} /> <span className="text-xs">8 Reviews</span></div>
  
    </div>
    <p class="text-xs text-gray-500"><a href="#" class="hover:underline hover:text-blue-500">username2019</a> • 2 days ago</p>
  </div>
                      </div>

                  </div>
                )
              })
            }
          </div>
        </div>

       

<OptimizelyFeature feature="showGmap">
          {(enabled, variables) => {
            console.log(enabled, variables);
            return (
              enabled ? (
                <div className="right-container w-1/3 p-1 m-1">
                <MarkSearchResult />
              </div>
              ) : null
            )
          }
          
          }
        </OptimizelyFeature>


      </div>



    </div>
  );
}
export default SearchResult;