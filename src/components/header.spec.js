import React from "react";
import { render } from '@testing-library/react'
import Header from "./header";

describe("Header component",()=>{
 // const headerComponent = null;
 // beforeEach(()=>{
 //  headerComponent = render(<Header/>);
 // })

  it('it should render without fail',()=>{
   const {getByText} =  render(<Header/>);
    expect(getByText('Features')).toBeInTheDocument();
  })
})