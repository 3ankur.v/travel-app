import React from "react";
import logo from "../assets/images/logo.svg";
import trvImg from "../assets/images/beach-work.jpg";

function Test(){

 return(
  <div className="px-8 py-10">
   <img className="h-10" alt={"logo"} src={logo} />
   <img className="mt-4 rounded-lg shadow-xl" alt={"beach"} src={trvImg} />

  </div>
 );
}
export default Test;