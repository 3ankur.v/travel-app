import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";



function TopCountriesList() {
 const topCountriesList = [
  {
   name:"New York",
   price:"£ 125 per/day",
   imgUrl:"http://makent.trioangle.com/images/home_cities/home_city_1569501592.jpeg",
  },
  {
   name:"Sydney",
   price:"£ 105 per/day",
   imgUrl:"http://makent.trioangle.com/images/home_cities/home_city_1569501876.jpeg",
  },
  {
   name:"Hawaii",
   price:"£ 105 per/day",
   imgUrl:"http://makent.trioangle.com/images/home_cities/home_city_1569502184.jpeg",
  },
  {
   name:"Parise",
   price:"£ 105 per/day",
   imgUrl:"http://makent.trioangle.com/images/home_cities/home_city_1569504637.jpeg",
  },
  {
   name:"Barcelona",
   price:"£ 105 per/day",
   imgUrl:"http://makent.trioangle.com/images/home_cities/home_city_1569503962.jpeg",
  },
  {
   name:"ThaiLand",
   price:"£ 105 per/day",
   imgUrl:"http://makent.trioangle.com/images/home_cities/home_city_1569579022.jpeg",
  },
  {
   name:"London",
   price:"£ 105 per/day",
   imgUrl:"http://makent.trioangle.com/images/home_cities/home_city_1569579115.jpeg",
  },
  {
   name:"San Francisco",
   price:"£ 105 per/day",
   imgUrl:" http://makent.trioangle.com/images/home_cities/home_city_1569505132.jpeg",
  },{
  

   name:"Berlin",
   price:"£ 134 per/day",
   imgUrl:"http://makent.trioangle.com/images/home_cities/home_city_1569505210.jpeg",
  }
 ];
 const settings = {
  dots: true,
  infinite: false,
  speed: 500,
  slidesToShow: 4,
  slidesToScroll: 4,
  initialSlide: 0,
  responsive: [
   {
    breakpoint: 1024,
    settings: {
     slidesToShow: 3,
     slidesToScroll: 3,
     infinite: true,
     dots: true
    }
   },
   {
    breakpoint: 600,
    settings: {
     slidesToShow: 2,
     slidesToScroll: 2,
     initialSlide: 2
    }
   },
   {
    breakpoint: 480,
    settings: {
     slidesToShow: 1,
     slidesToScroll: 1
    }
   }
  ]
 };


  
  return (
   <div>
    <h2> Recommended For You </h2>
    <Slider {...settings}>
   {
    topCountriesList.map(({name,price,imgUrl},idx)=>{
      return(
       <div className="relative p-2" key={idx}>
       <img className="mt-4 w-full top-countries rounded-lg  object-cover" alt="beach" src={imgUrl} />
 
       <div className="text-center relative z-10 w-full ct-name">
      <h2 className="text-2xl  font-medium title-font mb-2">{name}</h2>
       </div>
      </div>
      )
    })
   }
    </Slider>
   </div>
  );
 
}

export default TopCountriesList ;