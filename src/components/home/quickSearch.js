import React,{useState, useEffect} from "react"
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import places from "places.js";

import { DateRangePicker } from 'react-dates';


// startDate: null,
// endDate: null,
// focusedInput: null,


function QuickSearch(props){

  const[startDate,setStartDate] = useState(null);
  const[endDate,setEndDate] = useState(null);
  const[focusedInput,setFocusedInput] = useState(null);


  useEffect(()=>{
    var placesAutocomplete = places({
        appId: 'plG6LKX39ST7',
        apiKey: 'db3962084cbc477fac7cd2c7d5c8af5a',
        container: document.querySelector('#place-name')
      });

      placesAutocomplete.on('change', function(e) {
       // $address.textContent = e.suggestion.value
       console.log("Chanhing value", e);
      });
    
      placesAutocomplete.on('clear', function() {
       // $address.textContent = 'none';
       console.log("Cleaning value");
      });
  },[])

  const findResult = () =>{
    props.history.push('/search?location=london');
  }

  // lg:absolute md:static  bottom-0 left-0  px-4  mx-auto flex
  return(
   <div className="lg:absolute md:relative px-5 bottom-0 left-0 mx-auto flex lg:w-1/4">
    <div className="bg-white rounded-lg p-8 flex flex-col md:ml-auto w-full mt-10 md:mt-0 relative z-10">
      <h2 className="text-gray-900 text-lg mb-1 font-medium title-font"><span style={{color:"#ff5a5f"}}>Explore the world!</span> Rent suitable homes and experience your trips at an affordable cost.</h2>
     <label className="text-sm">Where</label>
      <input id="place-name" className="bg-white rounded border border-gray-400 focus:outline-none focus:border-indigo-500 text-base px-4 py-2 mb-4" placeholder="Where" type="text" />
      {/* <input className="bg-white rounded border border-gray-400 focus:outline-none focus:border-indigo-500 text-base px-4 py-2 mb-4" placeholder="When" type="text" />
      */}
<label className="text-sm">When</label>
<DateRangePicker
          startDateId="startDate"
          endDateId="endDate"
          startDate={startDate}
          endDate={endDate}
          onDatesChange={({ startDate, endDate }) =>{ setStartDate(startDate); setEndDate(endDate)}}
          focusedInput={focusedInput}
          onFocusChange={(focusedInput) => { setFocusedInput(focusedInput)}}
          
        />

<label className="text-sm">Guest</label>
<select className="bg-white rounded border border-gray-400 focus:outline-none focus:border-indigo-500 text-base px-4 py-2 mb-4" id="grid-state">
          <option>1 Guest</option>
          <option>2 Guests</option>
          <option>3 Guests</option>
          <option>4 Guests</option>
          <option>5 Guests</option>
        </select>
      <button  onClick={findResult} className="text-white bg-indigo-500 border-0 py-2 px-6 focus:outline-none hover:bg-indigo-600 rounded text-lg">Search</button>
      {/* <p className="text-xs text-gray-500 mt-3">Chicharrones blog helvetica normcore iceland tousled brook viral artisan.</p> */}
    </div>
  </div>
  )
}

export default QuickSearch;