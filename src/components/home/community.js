import React from "react";

function Community() {

 return (
  <div className="container">
   
   <div class="grid grid-cols-2 gap-4">

    <div class="p-4 sm:mb-0 mb-6">
     <div class="rounded-lg h-64 overflow-hidden">
      <img alt="content" class="object-cover object-center h-full w-full" src="https://demo.rentallscript.com/images/home/b8a18a2297930f25204ed39b45e8ffef.jpeg" /></div>
      <h6 style={{color:"#974121"}} class="font-medium title-font text-gray-900 mt-3">Enjoy your trip!</h6>
      <p class="text-base leading-relaxed mt-1">
      Rent the home that is suitable for you & your family and enjoy your trip!</p>
      <a class="text-indigo-500 inline-flex items-center mt-3">Learn More
      
      <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-2" viewBox="0 0 24 24"><path d="M5 12h14M12 5l7 7-7 7"></path></svg></a>
      
      </div>


      <div class="p-4 sm:mb-0 mb-6">
     <div class="rounded-lg h-64 overflow-hidden">
      <img alt="content" class="object-cover object-center h-full w-full" src="https://demo.rentallscript.com/images/home/da398bc9d13fe6d332a2e70d90740289.jpeg" /></div>
      <h6 style={{color:"#974121"}} class="font-medium title-font text-gray-900 mt-3">Trusted community!</h6>
      <p class="text-base leading-relaxed mt-1">
      Our community is completely driven by trust and your family safety is assured!</p>
      <a class="text-indigo-500 inline-flex items-center mt-3">Learn More
     
      <svg fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="w-4 h-4 ml-2" viewBox="0 0 24 24"><path d="M5 12h14M12 5l7 7-7 7"></path></svg></a>
      </div>
   </div>
  </div>
 );
}
export default Community;