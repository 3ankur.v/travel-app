import React from "react";
import Rater from 'react-rater'
import 'react-rater/lib/react-rater.css'
const details = [
    {
        name: 'Private Room ',
        imgUrl: 'https://demo.rentallscript.com/images/upload/x_small_4822b95672fd826182c0ff181186f984.jpeg',
        price: '£ 191 Per Night',
        rating: '',
        info: {
            roomType: 'Chalet',
            bedInfo: '8 Beds',
            totalReview: 8
        }
    }
]
function RecentBooked() {
    // background-image: url('https://images.unsplash.com/photo-1495856458515-0637185db551?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=750&amp;q=80')
    return (
        <div className="grid gap-6 grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 mt-6">
            <div className="w-full max-w-sm mx-auto rounded-md shadow-md overflow-hidden">
                <div className="flex items-end justify-end h-56 w-full bg-cover"
                    style={{ backgroundImage: 'url("https://images.unsplash.com/photo-1495856458515-0637185db551?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=750&amp;q=80")' }}>
                    <button className="p-2 rounded-full  text-white mx-5 -mb-0 hover:bg-blue-500 focus:outline-none focus:bg-blue-500">
                        {/* <svg className="h-5 w-5" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"></path></svg> */}
                        <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fillRule="evenodd" clipRule="evenodd"><path d="M12 21.593c-5.63-5.539-11-10.297-11-14.402 0-3.791 3.068-5.191 5.281-5.191 1.312 0 4.151.501 5.719 4.457 1.59-3.968 4.464-4.447 5.726-4.447 2.54 0 5.274 1.621 5.274 5.181 0 4.069-5.136 8.625-11 14.402m5.726-20.583c-2.203 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248-3.183 0-6.281 2.187-6.281 6.191 0 4.661 5.571 9.429 12 15.809 6.43-6.38 12-11.148 12-15.809 0-4.011-3.095-6.181-6.274-6.181" /></svg>

                    </button>
                </div>
                <div className="px-5 py-3">
                    <h3 className="text-gray-700 uppercase">Private Room - 1 Bed</h3>
                    <span className="text-gray-500 mt-2">$123 Per night</span>
                    <div><Rater total={5} rating={2} /> <span className="text-xs">8 Reviews</span></div>
                </div>
            </div>

            <div className="w-full max-w-sm mx-auto rounded-md shadow-md overflow-hidden">
                <div className="flex items-end justify-end h-56 w-full bg-cover"
                    style={{ backgroundImage: 'url("https://demo.rentallscript.com/images/upload/x_small_71b7740bd077704d3b33f97c5b06ec6b.jpeg")' }}>
                    <button className="p-2 rounded-full  text-white mx-5 -mb-4 hover:bg-blue-500 focus:outline-none focus:bg-blue-500">
                        {/* <svg className="h-5 w-5" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor"><path d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"></path></svg> */}
                        <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fillRule="evenodd" clipRule="evenodd"><path d="M12 21.593c-5.63-5.539-11-10.297-11-14.402 0-3.791 3.068-5.191 5.281-5.191 1.312 0 4.151.501 5.719 4.457 1.59-3.968 4.464-4.447 5.726-4.447 2.54 0 5.274 1.621 5.274 5.181 0 4.069-5.136 8.625-11 14.402m5.726-20.583c-2.203 0-4.446 1.042-5.726 3.238-1.285-2.206-3.522-3.248-5.719-3.248-3.183 0-6.281 2.187-6.281 6.191 0 4.661 5.571 9.429 12 15.809 6.43-6.38 12-11.148 12-15.809 0-4.011-3.095-6.181-6.274-6.181" /></svg>

                    </button>
                </div>
                <div className="px-5 py-3">
                    <h3 className="text-gray-700 uppercase">Classic watch</h3>
                    <span className="text-gray-500 mt-2">$123</span>

                </div>
            </div>
        </div>
    )
}

export default RecentBooked;