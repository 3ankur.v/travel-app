import React from "react";
import Responsive from "./topCountries";
import RecentBooked from "./recentBooked";
import Community from "./community";
import Support from "./support";

function HomeView() {
   return (
      <>
         <div className="container px-5 py-10 mx-auto">
            {/* <div className="flex flex-wrap -m-4">
    <div className="max-w-sm rounded overflow-hidden shadow-lg  ">
     <img className="w-full" src="https://res.cloudinary.com/makent-trioangle/image/upload/c_fill/v1/Makent/a4zdb9m86p0pwat2u55i" alt="Sunset in the mountains" />
     <div className="px-6 py-4">
      <div className="font-bold text-xl mb-2">Stays</div>
     </div>
    </div>

    <div className="max-w-sm rounded overflow-hidden shadow-lg  ">
     <img className="w-full" src="https://res.cloudinary.com/makent-trioangle/image/upload/c_fill/v1/Makent/isolqjioqzjejkx8iajg" alt="Sunset in the mountains" />
     <div className="px-6 py-4">
      <div className="font-bold text-xl mb-2">Experiences</div>
     </div>
    </div>
   </div> */}


            {/* <div className="flex flex-wrap -m-4">
<div className="flex  w-1/2 p-4">
   <img className="lg:h-20 w-full md:w-1/3  object-cover rounded-lg rounded-r-none pb-5/6" src="https://res.cloudinary.com/makent-trioangle/image/upload/c_fill/v1/Makent/a4zdb9m86p0pwat2u55i" alt="bag" />
   <div className="w-full md:w-1/2 px-4 py-4 bg-white rounded-lg">
      <div className="flex items-center">
         <h2 className="text-xl text-gray-800 font-medium mr-auto">Stays</h2>
      </div>
   </div>
</div>

<div className="flex  w-1/2 p-4">
   <img className="lg:h-20 w-full md:w-1/3  object-cover rounded-lg rounded-r-none pb-5/6" src="https://res.cloudinary.com/makent-trioangle/image/upload/c_fill/v1/Makent/isolqjioqzjejkx8iajg" alt="bag" />
   <div className="w-full md:w-1/2 px-4 py-4 bg-white rounded-lg">
      <div className="flex items-center">
         <h2 className="text-xl text-gray-800 font-medium mr-auto">Experiences</h2>
      </div>
   </div>
</div>
</div> */}

            <div className="py-10">
               <Responsive />
            </div>

            <div className="py-8">
               <h4>Most Viewed</h4>
               <RecentBooked />
            </div>

            <div className="py-5">
               <h4 className="text-xl font-bold">
                  Rent amazing homes for your trip
 </h4>
               <span>Homes with high standards and better facilities</span>
               <Community />
            </div>





         </div>
         <div className=" ">
            <Support />
         </div>
      </>
   )
}

export default HomeView;