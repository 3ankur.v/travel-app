import React from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

function CarouselHome(){
 return(
  <Carousel infiniteLoop={true} showArrows={false} showIndicators={false} showThumbs={false} dynamicHeight={true}>
  <div>
      <img src="http://makent.trioangle.com/images/slider/home_page_slider_1569578869.jpg" />
  </div>
  <div>
      <img src="http://makent.trioangle.com/images/slider/home_page_slider_1569578860.jpg" />
  </div>
  <div>
      <img src="http://makent.trioangle.com/images/slider/home_page_slider_1569578840.jpg" />
  </div>
</Carousel>
 )
}
export default CarouselHome;