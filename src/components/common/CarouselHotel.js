import React from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

function CarouselHotel({imagesList}){
 return(
  <Carousel infiniteLoop={false} showArrows={true} showThumbs={false} dynamicHeight={true}>
   {
    imagesList.map((item)=>{
      return(
       <div key={item.id}>
       <img src={item.url} alt={item.name} />
   </div>
      )
    })
   }
</Carousel>
 )
}
export default CarouselHotel;